<style>
  .disabled {
    border: none;
  }

  .endable {
      border: 1px;
  }

</style>

TODO:<input type="text" style="width:200px" name="todo_list" id="todo_list" placeholder="Enter what you want to do here...">
<button type="button" onclick="Add()">Add!</button>

<div id="show_result">
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script>
var id_change_global = 0;
$(document).ready(function() {
    init_table();
});
function init_table()
{
    var html = "";
    var store_value = localStorage.getItem("todo_list");
        store_value = JSON.parse(store_value)
    for(var i = 0; i < store_value.length; i++)
    {
        html += "<table style='width:500px;' border='1'>";
        html += "<tr>";
        if(store_value[i]['status'] == 1)
        {
        html += "<td style='width:50%'>"+(i+1)+".<input class='input-t disabled' id='input_"+i+"' readonly value="+store_value[i]['todo_list']+"></input></td>";
        }
        else
        {
        html += "<td style='text-decoration: line-through;width:50%;'>"+(i+1)+"."+store_value[i]['todo_list']+"</td>";    
        }
        if(store_value[i]['status'] == 1)
        {
        html += "<td><button onclick='Done("+i+")'>Done</button> | <button onclick='Edit("+i+")'>Edit</button> | <button onclick='Remove("+i+")'>Remove</button> </td>";
        } else
        {
            html += "<td><button>Remove</button></td>";
        }
        html += "</tr>";
        html += "</table>";
    }
    $("#show_result").html(html);
}

function Add()
{
var todo_list = $("#todo_list").val();
var array_storage_todo = []
var check_store_value = localStorage.getItem("todo_list");
check_store_value = JSON.parse(check_store_value)
if(check_store_value.length > 0)
{
    check_store_value.push({"todo_list": todo_list,status : 1})
    localStorage.setItem("todo_list", JSON.stringify(check_store_value));
} else
{
array_storage_todo.push({"todo_list": todo_list,status : 1})
localStorage.setItem("todo_list", JSON.stringify(array_storage_todo));

}
init_table();
}

function Remove(id)
{
    var store_value = localStorage.getItem("todo_list");
        store_value = JSON.parse(store_value)
    for(var i = 0; i < store_value.length; i++)
    {
        if(id == i)
        {
            store_value[i]['status'] = 0;
        }
    }
    localStorage.setItem("todo_list", JSON.stringify(store_value));
    init_table();
}

function Edit(id)
{
    $(".input-t").addClass('disabled')
    $('.input-t').prop('readonly', true);
    $("#input_"+id+"").removeClass('disabled')
    $("#input_"+id+"").prop('readonly', false);
    id_change_global = id;
}

function Done(id)
{   
    if(id_change_global == id)
    {
    var value_change = $("#input_"+id+"").val();
    var store_value = localStorage.getItem("todo_list");
        store_value = JSON.parse(store_value)
    for(var i = 0; i < store_value.length; i++)
    {
        if(id == i)
        {
            store_value[i]['todo_list'] = value_change;
        }
    }
    localStorage.setItem("todo_list", JSON.stringify(store_value));
    init_table();
    }
    else
    {
        return;
    }
}

</script>
The number is 209324<br>
<br>
<div style="display: flex;">
<p>Output Value :
</div>

<div id="show_result">
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script>
$(document).ready(function() {
    var n = "209324";
    var array_number = (""+n).split("");
    var array_result = perm(array_number)
    var output_result = []
    var text_result = "";
    for(var i =0; i < array_result.length; i++)
    {
        var result_show = "";
        for(var m = 0; m < array_result[i].length; m ++)
        {
            result_show += array_result[i][m]
        }
        output_result.push(result_show);
    }

    for(var ma=0; ma < output_result.length; ma++)
        {
            text_result += "<div>"+output_result[ma]+"</div>";
        }
        $("#show_result").html(text_result);
});

function perm(xs) {
  let ret = [];

  for (let i = 0; i < xs.length; i = i + 1) {
    let rest = perm(xs.slice(0, i).concat(xs.slice(i + 1)));

    if(!rest.length) {
      ret.push([xs[i]])
    } else {
      for(let j = 0; j < rest.length; j = j + 1) {
        ret.push([xs[i]].concat(rest[j]))
      }
    }
  }
  return ret;
}
</script>